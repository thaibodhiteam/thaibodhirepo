$(document).ready(function() {

	$('head').append('<link rel="stylesheet" type="text/css" href="assets/styleswitcher/styleswitcher.css">');
	$body.append('<div id="style-switcher"></div>');
	$('#style-switcher').load('assets/styleswitcher/styleswitcher.html', function(){

		var activeScheme = $('#theme').attr("href").substring(18); 
		activeScheme = activeScheme.substring(0,activeScheme.length-4); 
		activeScheme = activeScheme.split('-');

		var activeColor = activeScheme[1];

		var $colorSwitcher = $('#color-switcher'),
			$layoutSwitcher = $('#layout-switcher');

		$colorSwitcher.find('a').each(function() {
	        if($(this).data('color')==activeColor) $(this).addClass('active');
	    });
		
	    if($body.hasClass('body-boxed')) $layoutSwitcher.find('a[data-layout="boxed"]').addClass('active');
	    else if($body.hasClass('body-padded')) $layoutSwitcher.find('a[data-layout="padded"]').addClass('active');
	    else $layoutSwitcher.find('a[data-layout="fullwidth"]').addClass('active');

		$('#style-switcher ul').find('a').on('click', function(){
			$(this).parents('ul').find('a.active').removeClass('active');
			$(this).addClass('active');
			return false;
		});

		$layoutSwitcher.find('a').on('click', function(){
			if($(this).data('layout')=='fullwidth') $body.removeClass('body-padded').removeClass('body-boxed').css('background-image','');
			else if($(this).data('layout')=='boxed') $body.removeClass('body-padded').addClass('body-boxed').css('background-image','url(assets/img/photos/advisior_bg01.jpg)');
			else if($(this).data('layout')=='padded') $body.removeClass('body-boxed').addClass('body-padded').css('background-image','');

			$('.carousel, .bg-slideshow').each(function(){
				var item = $(this).data('owlCarousel');
				item.destroy();
			});

			Zysk.Component.carousel();
			$('.bg-slideshow').owlCarousel({
                singleItem: true,
                autoPlay: 4000,
                pagination: false,
                navigation: false,
                navigationText: false,
                slideSpeed: 1500,
                transitionStyle: 'fade',
                mouseDrag: false,
                touchDrag: false
            });
			return false;
		});

		$colorSwitcher.find('a').on('click', function(){
			activeColor = $(this).data('color');
			$('#theme').attr('href','assets/css/themes/theme-'+activeColor+'.css');
			return false;
		});

		$('#style-switcher .ss-toggle').click(function(){
			var div = $('#style-switcher');
			if (div.css('right') === '-175px') {
				$('#style-switcher').animate({
					right: '0'
				});
				$(this).toggleClass('active');
			} else {
				$('#style-switcher').animate({
					right: '-175px'
				});
				$(this).toggleClass('active');
			}
			return false;
		});

	});

});
